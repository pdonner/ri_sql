/*
This is a demonstration of an implementation of Random Indexing in SQL and PL/pgSQL for multilingual 
information retrieval, specifically cross-language text similarity calculation.

In this demo, no real data will be used.

Requirements:
  PostgreSQL 12 or higher and the following extensions: aggs_for_vecs, floatvec, aggs_for_arrays.
  These can be obtained from pgxn.org and installed (under Linux) with the pgxnclient tool.

  Some multilingual parallel corpus, i.e. translated documents, for training and possibly a seperate corpus of 
documents for which to calculate similarities.

*/

-- install the extensions in pg:

CREATE EXTENSION aggs_for_vecs;
CREATE EXTENSION floatvec;
CREATE EXTENSION aggs_for_arrays;

-- function to create sparse ternary index vectors
CREATE OR REPLACE FUNCTION rand_vec(d integer) RETURNS int4[]
AS $$
DECLARE
	vector int4[] := array_fill(0, ARRAY[d]);
	n int4 := 1 / (sqrt(d) * 2) * d; -- number of non-null values
	nn_ind int4[]; -- non-null indexes
BEGIN
	SELECT ARRAY(
		SELECT ind
		FROM GENERATE_SERIES (1, d) AS i(ind)
		ORDER BY random() 
		LIMIT n * 2
	) INTO nn_ind;	
	FOR i IN 1 .. array_upper(nn_ind, 1) LOOP
		IF i <= n
		  THEN vector[nn_ind[i]] := -1;
		  ELSE vector[nn_ind[i]] := 1;
		END IF;
	END LOOP;
	RETURN(vector);
END;
$$
LANGUAGE plpgsql PARALLEL SAFE;

-- function for L2 normalization of vectors
CREATE OR REPLACE FUNCTION l2_normalize(v real[]) RETURNS real[]
AS $$
DECLARE
	nv real[];
BEGIN
	nv := vec_div(v, 
	  (
	    SELECT SQRT(SUM(elem))
	    FROM (
	      SELECT UNNEST(vec_mul(v, v)) elem
	    ) a )::real);
	RETURN(nv);
END;
$$
LANGUAGE plpgsql IMMUTABLE STRICT PARALLEL SAFE;

-- function to calculate the dot product of two vectors
CREATE OR REPLACE FUNCTION dotprod(a real[], b real[]) RETURNS real
AS $$
DECLARE
	card_a integer := cardinality(a);
	card_b integer := cardinality(b);
	res real := 0;
BEGIN
	IF card_a != card_b 
	THEN RAISE EXCEPTION 'arrays must have same cardinality. array 1 is %, array 2 is %', card_a, card_b;
	END IF;
	FOR x IN 1 .. card_a
	LOOP
		res := res + (a[x] * b[x]);
	END LOOP;
	RETURN(res);
END;
$$
LANGUAGE plpgsql IMMUTABLE STRICT PARALLEL SAFE;

/*
In the example used to demonstrate this implementation, I will use seperate training data (translated documents) 
and target data (documents for which similarities are to be computed). In principle, these could be all within 
one corpus and one could use less tables. In the example, there are two languages, 'de' and 'en'.
*/

-- this table needs to contain the training data corpus
CREATE TABLE training_docs (
  id text PRIMARY KEY,
  de text[],
  en text[],
  i_vector int4[]
);

-- load the data. it should consist of an ID, and properly delimited terms in two languages

/* 
initialize the index vectors for the training document
the dimensionality of the index vectors and the final vector space must be chosen at this 
point as the parameter value for the rand_vec() function. here, 1000 is used
*/
UPDATE training_docs
SET i_vector = rand_vec(1000);

-- indexes for faster lookups of terms
CREATE INDEX i_docs_en ON training_docs USING GIN(en);
CREATE INDEX i_docs_de ON training_docs USING GIN(de);

-- create a vocabulary table which keeps track of all term context vectors and occurrence frequencies
CREATE TABLE training_vocab (
  language CHAR(2),
  term text,
  term_collection_count int,
  c_vector real[], -- generated from i-vectors of documents that contain term
  n_docs int,
  PRIMARY KEY (language, term)
);

INSERT INTO training_vocab (language, term, term_collection_count, n_docs)
SELECT lang, term, COUNT(*) cnt, COUNT(DISTINCT id)
FROM (
  SELECT id, 'de' lang, UNNEST(de) term
  FROM training_docs
  UNION ALL
  SELECT id, 'en' lang, UNNEST(en) term
  FROM training_docs
) sub
WHERE term IS NOT NULL
GROUP BY lang, term;

-- index terms by their document i-vectors
-- raw vectors
UPDATE training_vocab
SET c_vector = (
  SELECT vec_to_sum(i_vector)::real[] vector
  FROM training_docs
  WHERE de @> ARRAY[training_vocab.term]
  )
WHERE language = 'de'
;
UPDATE training_vocab
SET c_vector = (
  SELECT vec_to_sum(i_vector)::real[] vector
  FROM training_docs
  WHERE en @> ARRAY[training_vocab.term]
  )
WHERE language = 'en'
;

-- note: this will count each term only once per document, to include document frequency, try something like this (not tested):
/*
UPDATE training_vocab
SET c_vector = (
	SELECT requiz.vec_to_sum(i_vector)::real[] vector
	FROM (
		SELECT id, UNNEST(de) de, i_vector
		FROM training_docs
		WHERE de @> ARRAY[training_vocab.term]
	) a
	WHERE language = training_vocab.term
)
WHERE language = 'de';

UPDATE training_vocab
SET c_vector = (
	SELECT requiz.vec_to_sum(i_vector)::real[] vector
	FROM (
		SELECT id, UNNEST(en) en, i_vector
		FROM training_docs
		WHERE en @> ARRAY[training_vocab.term]
	) a
	WHERE language = training_vocab.term
)
WHERE language = 'en';
*/


-- normalize (L2-norm)
UPDATE training_vocab
SET c_vector = l2_normalize(c_vector);


-- index documents by tf-idf
ALTER TABLE training_docs ADD COLUMN c_vector real[];
-- raw document index vectors
-- tf-idf average of term vectors

DO $$ 
DECLARE 
	corpus_size real;
	i int := 1;
	doc_id text;
BEGIN
	SELECT COUNT(*) INTO corpus_size FROM training_docs;
	FOR doc_id IN SELECT id FROM training_docs 
	LOOP
		RAISE NOTICE '%, %', i, doc_id;
		i := i + 1;
   	UPDATE training_docs
   	SET c_vector = (
   		SELECT vec_to_sum(vector)
			FROM (		
				SELECT  b.term, vec_mul(v.c_vector, (tf * LOG(corpus_size / v.n_docs::real))::real) vector
				FROM (
					SELECT term, lang, COUNT(*) tf
					FROM (
					  SELECT UNNEST(de) term, 'de' lang
					  FROM training_docs
					  WHERE id = doc_id
					  UNION ALL
					  SELECT UNNEST(en) term, 'en' lang
					  FROM training_docs
					  WHERE id = doc_id
					) a
					GROUP BY term, lang
				) b
				JOIN training_vocab v ON b.term = v.term AND b.lang::char(2) = v.language
			) b
   	)
   	WHERE id = doc_id;
	COMMIT; 	
	END LOOP;
END;
$$;

-- normalize
UPDATE training_docs
SET c_vector = l2_normalize(c_vector);

-- reflective indexing of terms (second order vectors)

ALTER TABLE training_vocab ADD COLUMN c_vector_o2 real[];
-- raw vectors
UPDATE training_vocab
SET c_vector_o2 = (
  SELECT vec_to_sum(c_vector)::real[] vector
  FROM training_docs
  WHERE en @> ARRAY[training_vocab.term]
  )
WHERE language = 'en'
;

UPDATE training_vocab
SET c_vector_o2 = (
  SELECT vec_to_sum(c_vector)::real[] vector
  FROM training_docs
  WHERE de @> ARRAY[training_vocab.term]
  )
WHERE language = 'de'
;

-- normalize
UPDATE training_vocab
SET c_vector_o2 = l2_normalize(c_vector_o2);

/*
This is the table for the target documents, i.e. for which similarity calculations will be made. 
This could also be more than one table. Documents should should have terms in at least one of the 
training languages.
*/

CREATE TABLE target_docs (
  id text PRIMARY KEY,
  en text[],
  de text[],
  c_vector real[],
  c_vector_o2 real[]
);

-- load the data into the first three columns

-- batch calculation of the target document representations
DO $$ 
DECLARE 
	corpus_size real;
	i int := 1;
	doc_id text;
BEGIN
	SELECT COUNT(*) INTO corpus_size FROM training_docs;
	FOR doc_id IN SELECT id FROM target_docs WHERE c_vector IS NULL
	LOOP
		RAISE NOTICE '%, %', i, doc_id;
		i := i + 1;
   	UPDATE target_docs
   	SET c_vector = (
   		SELECT vec_to_sum(vector)
			FROM (		
				SELECT  b.term, vec_mul(v.c_vector, (tf * LOG(corpus_size / v.n_docs::real))::real) vector
				FROM (
					SELECT term, lang, COUNT(*) tf
					FROM (
					  SELECT UNNEST(en) term, 'en' lang
					  FROM target_docs
					  WHERE id = doc_id
					  UNION ALL
					  SELECT UNNEST(de) term, 'de' lang
					  FROM target_docs
					  WHERE id = doc_id
					) a
					GROUP BY term, lang
				) b
				JOIN training_vocab v ON b.term = v.term AND b.lang::char(2) = v.language
			) b
   	)
   	WHERE id = doc_id;
	COMMIT; 	
	END LOOP;
END;
$$;

DO $$ 
DECLARE 
	corpus_size real;
	i int := 1;
	doc_id text;
BEGIN
	SELECT COUNT(*) INTO corpus_size FROM training_docs;
	FOR doc_id IN SELECT id FROM target_docs 
	LOOP
		RAISE NOTICE '%, %', i, doc_id;
		i := i + 1;
   	UPDATE target_docs
   	SET c_vector_o2 = (
   		SELECT vec_to_sum(vector)
			FROM (		
				SELECT  b.term, vec_mul(v.c_vector_o2, (tf * LOG(corpus_size / v.n_docs::real))::real) vector
				FROM (
					SELECT term, lang, COUNT(*) tf
					FROM (
					  SELECT UNNEST(en) term, 'en' lang
					  FROM target_docs
					  WHERE id = doc_id
					  UNION ALL
					  SELECT UNNEST(de) term, 'de' lang
					  FROM target_docs
					  WHERE id = doc_id
					) a
					GROUP BY term, lang
				) b
				JOIN training_vocab v ON b.term = v.term AND b.lang::char(2) = v.language
			) b
   	)
   	WHERE id = doc_id;
	COMMIT; 	
	END LOOP;
END;
$$;

UPDATE target_docs
SET c_vector = l2_normalize(c_vector), c_vector_o2 = l2_normalize(c_vector_o2);

-- calculate cosine sim and second order cosine sim
SELECT dotprod(dd.c_vector, sd.c_vector), dotprod(dd.c_vector_o2, sd.c_vector_o2)
FROM target_docs dd, target_docs sd
WHERE 
AND dd.id = 1 AND sd.id = 2; -- replace the ids